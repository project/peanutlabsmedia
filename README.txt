
-- SUMMARY --

This module provides a way to integrate with Peanutlabsmedia.The Peanut Labs Media Monetization Platform is designed specifically to work with
virtual currencies and goods in the new digital economy - be they social applications on Facebook, Myspace or Linkedin, or leading Games and
Gaming communities.

You can learn more about Peanut Labs at http://www.peanutlabsmedia.com/.

For a full description of the module, visit the project page:
  http://drupal.org/project/peanutlabsmedia

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/peanutlabsmedia
  
-- REQUIREMENTS --

Userpoints Module. http://drupal.org/project/userpoints

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  peanutlabsmedia module:
  
* Configure the peanutlabsmedia settings at admin/settings/peanutlabsmedia

* You can access the survey page to earn points from here. peanutlab-media/take-survey

-- CONTACT --

Current maintainers:
* Anil Sagar (anil614sagar) - http://drupal.org/user/402938

Drupal 6 Module is originally written and maintained by Anil Sagar.

This project has been sponsored by:
* Blisstering Solutions
  Specialized in consulting and planning of Drupal powered sites, development, theming, customization, and hosting
  to get you started. Visit http://www.blisstering.com for more information.






